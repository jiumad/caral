﻿<?php  
include('header.php');
session_start();
ob_start();
if(!isset($_SESSION['login'])){
	header('location:login_page.php');
}

?>
<div class="row">
<div class="col col-lg-3 col-md-3"></div>
<div id="div-panel" class="col col-lg-6 col-md-6 col-sm-12 col-xs-12 table-responsive">
<table border="1" style="padding:2px;margin:20px auto 0 auto;" class="table">
	<thead>
			<th>نام</th>
			<th>نام خانوادگی</th>
			<th>آی دی</th>
			<th>ایمیل</th>
			<th>پیام</th>
			<th>حذف</th>
		
	</thead>
	<tbody>
	<?php
			$con=mysqli_connect('localhost',$database_user,$database_pass,$database_name);
		   if (mysqli_connect_errno()){
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			mysqli_set_charset($con,"utf8");
		   $select_query = mysqli_query($con,"select * from messages ORDER BY id DESC");
		   
	while($record = mysqli_fetch_array($select_query)){	 
	    $id = $record["id"];
		$description=$record["description"];
		$first_name = $record["firstName"];
		$last_name = $record["lastName"];
	    $email = $record["email"];
		
		echo "<tr>";
			echo "<td>" . $first_name . "</td>";
			echo "<td>" . $last_name . "</td>";
			echo "<td>" . $id . "</td>";
			echo "<td>" . $email . "</td>";
			echo "<td>" . $description . "</td>";
			echo "<td><a class='delete_post' href='delete_message.php?d_id=".$id."'".">حذف</a></td>";
		echo "</tr>";
		}
		
		?>
		
		</tbody>
		</table>
		</div>
		<div class="col col-lg-3 col-md-3"></div>
</div>