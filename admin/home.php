﻿
<?php 
include("header.php"); 

session_start();
ob_start();
if(!isset($_SESSION['login'])){
header('location:login.php');
}

?>

<div id="main-page-content" class="container">
		
		
		<div class="row" style="">
			
			
			
				<div class="col col-lg-3 col-md-3"></div>
			
				<div id="div-panel" class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
					
					
					
					<!-- Filtering Panel -->
					<div class="panel panel-default">
						<div class="panel-heading"> فیلتر </div>
						<div id="filter-panel" class="panel-body">
							<form  action="filter.php" method="get" role="form">
								<div class="form-group ">
									
									
									<select id="select-ostan" class="form-control" name="province">
										<?php
											$connection = mysqli_connect('localhost',$database_user,$database_pass,$database_name);
											mysqli_set_charset($connection,"utf8");
											$query_string = mysqli_query($connection,"SELECT * FROM province");
											while($result = mysqli_fetch_array($query_string)){
												$province_id = $result['p_id'];
												$name = $result['name'];
												
												echo "<option value=".$province_id.">".$name."</option>";
											}
										?>
										
									</select>
									
									
									
									<select id="select-type" class="form-control" name="category">
										<?php 
											$query_string = mysqli_query($connection,"SELECT * FROM category");
												while($result = mysqli_fetch_array($query_string)){
													$category_id = $result['c_id'];
													$name = $result['name'];
													
													echo "<option value=".$category_id.">".$name."</option>";
												}
												mysqli_close();
										?>
									</select>
									
		
									
									<p>
										<label for="price" style="display: block;font-size:1.2em">قیمت (تومان)</label>
										<section style="margin-left:1.3em; display:inline-block;">
										<label for="price-from">از</label>
										<input name="from_price" type="number" id="price-from" class="form-control">
										</section>
										<section style="display:inline-block;">
										<label for="price-to">تا</label>
										<input name="to_price" type="number" id="price-to" class="form-control">
										</section>
									</p>
									<div id="price-range" style="margin-bottom:0.2em;"></div>
										
									
										
									<button name="submit" id="search-button-of-filter-panel" type="submit" class="btn btn-success pull-left">جستجو</button>
										
								</div>
							</form>
						</div>
					</div>
					
				</div>
				
				<div class="col col-lg-3 col-md-3"></div>
			
		</div>
		
		<div class="row" >
			<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					
					
						
						<?php
							$con=mysqli_connect('localhost',$database_user,$database_pass,$database_name);
						   if (mysqli_connect_errno()){
								echo "Failed to connect to MySQL: " . mysqli_connect_error();
							}
							mysqli_set_charset($con,"utf8");
						   $query = mysqli_query($con,"SELECT post.id, province.p_id, province.name, post.subject , 
							post.image , post.price , post.prov_id , post.date
						   FROM post 
						   INNER JOIN province ON post.prov_id=province.p_id ORDER BY id DESC");
						   
							while($result = mysqli_fetch_array($query)){	 
								$id = $result["id"];
								$subject = $result["subject"];
								$image = $result["image"];
								$price = $result["price"];
								$province = $result["name"];
								$date = $result["date"];
								
								
								echo "<div class='mainpage-agahi-effect mainpage-agahi col col-lg-4 col-md-4 col-sm-6 col-xs-12'>";
									echo '<div class="row">';
										echo "<a href='post.php?postid=".$id."'>";
											echo "<img class='col col-lg-12 col-md-12 col-sm-12 col-xs-12' src='../$image' alt='car1' height='200em'>";
											
									    echo '</a>';
									echo'<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">';
										echo '<div class="row">';
											echo '<div class="details-container col col-lg-12 col-md-12 col-sm-12 col-xs-12">';
												echo "<div class='price-of-mainpage-agahi'>$price تومان</div>";	
												echo "<div class='name-of-mainpage-agahi'>$subject | $province</div>";
												echo "<div class='description-of-mainpage-agahi'>$date</div>";
												echo "<a href='delete_post.php?d_id=$id' ><div style='color:red' class='description-of-mainpage-agahi'>حذف</div></a>";
											echo'</div>';
										echo'</div>';
									echo'</div>';
								echo'</div>';
							echo'</div>';
								
							}
						?>
					
						
						
					</div>
				</div>	
			</div>
		
		</div>
		
		

 
 
 


	</div>	
	