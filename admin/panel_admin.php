﻿
<?php 
include("header.php"); 

session_start();
ob_start();
if(!isset($_SESSION['login'])){
header('location:login.php');
}

?>

<div id="main-page-content" class="container">
		
		
		<div class="row" style="">
			
			
				<div id="div-panel" class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
					<!-- Filtering Panel -->
					<div class="panel panel-default">
						<div class="panel-heading">اضافه کردن دسته بندی</div>
						<div id="filter-panel" class="panel-body">
							<form  action="add_category.php" method="post" role="form">
								<div class="form-group">
									
										<input type="number" name="category_id" class="form-control" id="select-ostan" placeholder="آیدی دسته بندی">
										<input type="text" name="category_name" class="form-control" id="select-type" placeholder="نام دسته بندی">
									
										<p style="font-size:13px" class="err">
										<?php 
										if(isset($_GET['reslut'])){
											if($_GET['reslut']==1){
												echo "دسته بندی جدید با موفقیت اضافه شد";
											}elseif($_GET['reslut']==0){
												echo "در فرآیند اضافه کردن دسته بندی مشکلی به وجود آمده";
											}elseif($_GET['reslut']==2){
												echo "آیدی باید بزرگ تر از 0 باشد";
											}elseif($_GET['reslut']==3){
												echo "آیدی قبلا وجود داشته لطفا از عدد دیگری استفاده کنید";
											}else{
												echo "";
											}
										}
										?>
										</p>
									
									<button name="add_submit" id="search-button-of-filter-panel" type="submit" class="btn btn-success pull-left">اضافه</button>
										
								</div>
							</form>
						</div>
					</div>
					
				</div>
				
				<div id="div-panel" class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
					<!-- Filtering Panel -->
					<div class="panel panel-default">
						<div class="panel-heading"> حذف کردن دسته بندی </div>
						<div id="filter-panel" class="panel-body">
							<form  action="delete_category.php" method="post" role="form">
								<div class="form-group">
											
									<select id="select-type" class="form-control" name="category">
										<?php 
										echo "1";
											$connection = mysqli_connect('localhost',$database_user,$database_pass,$database_name);
											mysqli_set_charset($connection,"utf8");
											$query_string = mysqli_query($connection,"SELECT * FRom category");
												while($result = mysqli_fetch_array($query_string)){
													$category_id = $result['c_id'];
													$name = $result['name'];
													
													echo "<option value=".$category_id.">".$category_id." ".$name."</option>";
												}
												mysqli_close();
										?>
									</select>
									<p style="font-size:13px" class="err">
										<?php 
										if(isset($_GET['res'])){
											if($_GET['res']==1){
												echo "حذف دسته بندی موفق بود";
											}else{
												echo "در فرآیند حذف دسته بندی مشکلی به وجود آمده";
											}
										}
										?>	
									</p>	
		
									<button name="del_submit" id="search-button-of-filter-panel" type="submit" style="border:2px solid red" class="btn btn-danger pull-left">حذف</button>
										
								</div>
							</form>
						</div>
					</div>
					
				</div>
			
		</div>
		
		
		</div>
		
		

 
 
 


	</div>	
	