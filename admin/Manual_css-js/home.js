$(document).ready(function(){

			//jquery Begining
			
			$("#register-header , #register-of-footer-of-modal").click(function(){
				$('#email-of-register-form').val('');
				$('#email-of-register-form').attr('placeholder','ایمیل');
				$('#password-of-register-form').val('');
				$('#password-of-register-form').attr('placeholder','رمز عبور');
				$('#confpassword-of-register-form').val('');
				$('#confpassword-of-register-form').attr('placeholder','تکرار رمز عبور');
				$('#phone-of-register-form').val('');
				$('#phone-of-register-form').attr('placeholder','تلفن  همراه');
			});
			
			$("#login-header , #login-of-footer-of-modal").click(function(){
				$('#email-of-login-form').val('');
				$('#email-of-login-form').attr('placeholder','ایمیل');
				$('#password-of-login-form').val('');
				$('#password-of-login-form').attr('placeholder','رمز عبور');
			});
			//carousel speed
			$('#myCarousel').carousel({
				interval: 10000
			});
			
			//activate indicator
			$('.indicator-image').click(function(){
				$('.indicator-image').removeClass('indicator-active');
				$(this).addClass('indicator-active');
			});
			
			//activate footer-item
			$('.footer-item').click(function(){
				$('.footer-item').removeClass('footer-active-item');
				$(this).addClass('footer-active-item');
			});
			
			
			//change ostan & city
			if($('#select-ostan').val() == "استان"){
					$("#select-city option").remove();
					$('#select-city').attr('disabled', true);
					$("<option>شهر</option>").appendTo('#select-city');
				}
			
			$('#select-ostan').change(function(){
				if($('#select-ostan').val() == "استان"){
					$("#select-city option").remove();
					$('#select-city').attr('disabled', true);
					$("<option>شهر</option>").appendTo('#select-city');
				}
			
				if($('#select-ostan').val() == "تهران"){
					$("#select-city option").remove();
					$('#select-city').attr('disabled', false);
					$("<option>شهر</option>").appendTo('#select-city');
					$("<option>اسلامشهر</option>").appendTo('#select-city');
					$("<option>تهران</option>").appendTo('#select-city');
					$("<option>دماوند</option>").appendTo('#select-city');
				}
				
				if($('#select-ostan').val() == "البرز"){
					$("#select-city option").remove();
					$('#select-city').attr('disabled', false);
					$("<option>شهر</option>").appendTo('#select-city');
					$("<option>کرج</option>").appendTo('#select-city');
				}
				
				if($('#select-ostan').val() == "اصفهان"){
					$("#select-city option").remove();
					$('#select-city').attr('disabled', false);
					$("<option>شهر</option>").appendTo('#select-city');
					$("<option>اصفهان</option>").appendTo('#select-city');
					$("<option>شاهین شهر</option>").appendTo('#select-city');
					$("<option>خمینی شهر</option>").appendTo('#select-city');
					$("<option>نجف آباد</option>").appendTo('#select-city');
				}
				
				if($('#select-ostan').val() == "فارس"){
					$("#select-city option").remove();
					$('#select-city').attr('disabled', false);
					$("<option>شهر</option>").appendTo('#select-city');
					$("<option>شیراز</option>").appendTo('#select-city');
					$("<option>ابرکوه</option>").appendTo('#select-city');
					$("<option>آباده</option>").appendTo('#select-city');
					$("<option>کازرون</option>").appendTo('#select-city');
				}
			});
			
			//change type
			if($('#select-type').val() == "نوع"){
					$("#select-brand option").remove();
					$('#select-brand').attr('disabled', true);
					$("<option>برند</option>").appendTo('#select-brand');
					$("#select-model option").remove();
					$('#select-model').attr('disabled', true);
					$("<option>مدل</option>").appendTo('#select-model');
				}
			$('#select-type').change(function(){
				if ($('#select-type').val() == "نوع"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', true);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', true);
				}
				
				if ($('#select-type').val() == "آمبولانس"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
				
				if ($('#select-type').val() == "اتوبوس"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
				
				if ($('#select-type').val() == "تاکسی"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
				
				if ($('#select-type').val() == "تصادفی"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
			
				if ($('#select-type').val() == "سواری"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
				
				if ($('#select-type').val() == "سنگین"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
				
				if ($('#select-type').val() == "گذر موقت"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
				
				if ($('#select-type').val() == "منطقه آزاد"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
				
				if ($('#select-type').val() == "وانت"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
				
				if ($('#select-type').val() == "ون"){
					$('#select-brand option').remove();
					$("<option>برند</option>").appendTo('#select-brand');
					$('#select-brand').attr('disabled', false);
					
					$('#select-model option').remove();
					$("<option>مدل</option>").appendTo('#select-model');
					$('#select-model').attr('disabled', false);
				}
			});
			
		});