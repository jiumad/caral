﻿<?php

session_start();
ob_start();
if(isset($_SESSION['login'])){
	if(session_destroy()){
		header('location:login.php');
	}
}

?>