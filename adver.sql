-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2017 at 09:37 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `adver`
--
CREATE DATABASE IF NOT EXISTS `adver` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `adver`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `c_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`c_id`, `name`) VALUES
(1, 'همه آگهی ها'),
(2, 'املاک'),
(3, 'وسایل نقلیه'),
(4, 'لوازم الکترونیکی'),
(5, 'وسایل خانگی'),
(6, 'وسایل شخصی'),
(7, 'آموزشی'),
(8, 'استخدام و کاریابی'),
(9, 'خدماتی'),
(10, 'اجتماعی');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `description` varchar(400) COLLATE utf8_bin NOT NULL,
  `firstName` varchar(50) COLLATE utf8_bin NOT NULL,
  `lastName` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `email`, `description`, `firstName`, `lastName`) VALUES
(2, 'ali@yahoo.com', 'تنسانتسیبایسبانسااااااااااااااااااااانستیباتنسیبانسیتبا', 'علی', 'نوری'),
(3, 'omahdi@j.co', 'منستسمینتبسمینتب', 'مهدی', 'فرجیان'),
(4, 'ss@f.com', 'lknwdkldnwkld', 'احمد', 'تهرانی'),
(5, 'a@p.net', 'نمتشیتشاسیاشسمتنیانشستیانشسا', 'عباس', 'عبدی'),
(6, 'a@m.com', 'ناشذینت  شسذیشنسیتذشسنتذ یشسنتذینشستتنذد', 'فیروز', 'دانشی'),
(7, 'A@g.com', 'تاناسی', 'kjsdfj', 'kljkjdjksd');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `phone` varchar(20) COLLATE utf8_bin NOT NULL,
  `subject` varchar(50) COLLATE utf8_bin NOT NULL,
  `description` varchar(400) COLLATE utf8_bin NOT NULL,
  `image` varchar(100) COLLATE utf8_bin NOT NULL,
  `price` int(11) NOT NULL,
  `date` date NOT NULL,
  `prov_id` int(11) NOT NULL,
  `ctg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prov_id` (`prov_id`,`ctg_id`),
  KEY `ctg_id` (`ctg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=47 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `email`, `phone`, `subject`, `description`, `image`, `price`, `date`, `prov_id`, `ctg_id`) VALUES
(39, 'ali@g.com', '09306932198', 'میز', 'میز تحریر اااااااااااااااااااااااااااااااااااااااااا اااااااااااااااااااااااااااااااااا ااااااااااااااااااااااااااااا اااااااااااااااااااااااااا ااااااااااااااااااااااااااا لللللللللللللللللل للللللللللللللللللللللللل لللللللللللللللل لللللللللللللللللللللللللل للللللللللللللللللل للللللللللللللللللللللللل للللللللللللللللللللللللل للللللللللللللللللللل للللللللللللل', 'images/N8Q Hxdymiz_333_1.jpg', 125000, '2017-06-21', 1, 1),
(41, 'ahmad@gmail.com', '03256598654', 'پونتیاک مشکی', 'جججججججججججججججججججج چچچچچچچچچچچچچچچچچچچچچچچ چچچچچچچچچچچچچچچچ چچچچچچچچچچچچچچ جججججججججججججج', 'images/Q9FRfVN1car1.jpg', 35000000, '2017-07-04', 9, 3),
(42, 'jaber@kk.com', '02546598741', 'لپ تاپ لنوو', 'لپتاپ پپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپ پپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپپ', 'images/ZXRj Gc_Notebook-Lenovo-IdeaPad-Z510-B0fe785.jpg', 2500000, '2017-07-04', 13, 4),
(43, 'mm@g.com', '09365489632', 'ظروف پلاستیکی', 'رررررررررررررررررررررررررررر ذذذذذذذذذذذذذذذذذذذذذذذ ذذذذذذذذذذذذذذذ ذذذذذذذذذذذذذذذذذذذذذذذذذذذذذذذ', 'images/7ZRp6yht222-w134227101550016e278b2f4.jpg', 6000, '2017-07-04', 15, 5),
(44, 'ttt@j.com', '09306548298', 'اکانت اینستاگرام', 'گگگگگگگگگگگگگگگگگگ', 'images/8Hj_gWFNinstagram.jpg', 500000, '2017-07-04', 26, 10),
(45, 'd@g.com', '02355654520', 'ززززززززززززززززززززز', 'رررررررررررررررررررررررررررررررررر', 'images/62FfmwPTgoogleplus.jpg', 25000000, '2017-07-05', 17, 2),
(46, 'A@b.com', '09125698741', 'اایتسلسی', 'اسی.تاسیتاسنس', 'images/9WPYcXMncar3.jpg', 125400, '2017-07-08', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE IF NOT EXISTS `province` (
  `p_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`p_id`, `name`) VALUES
(1, 'همه استان ها'),
(2, 'آذربایجان شرقی'),
(3, 'آذربایجان غربی'),
(4, 'اردبیل'),
(5, 'اصفهان'),
(6, 'البرز'),
(7, 'ایلام'),
(8, 'بوشهر'),
(9, 'تهران'),
(10, 'چهارمحال و بختیاری'),
(11, 'خراسان جنوبی'),
(12, 'خراسان شمالی'),
(13, 'خراسان رضوی'),
(14, 'خوزستان'),
(15, 'زنجان'),
(16, 'سمنان'),
(17, 'سیستان و بلوچستان'),
(18, 'فارس'),
(19, 'قزوین'),
(20, 'قم'),
(21, 'کردستان'),
(22, 'کرمان'),
(23, 'کرمانشاه'),
(24, 'کهگیلویه و بویراحمد'),
(25, 'گلستان'),
(26, 'گیلان'),
(27, 'لرستان'),
(28, 'مازندران'),
(29, 'مرکزی'),
(30, 'هرمزگان'),
(31, 'همدان'),
(32, 'یزد');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`) VALUES
(1, 'mohammad', 'mohammad@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`prov_id`) REFERENCES `province` (`p_id`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`ctg_id`) REFERENCES `category` (`c_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
