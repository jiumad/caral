<?php
	$database_name = 'adver';
	$database_user = 'root';
	$database_pass = '1234';

 ?>
<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
  <title>Caral || کارال</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css"/>
  <link rel="stylesheet" href="css/bootstrap-rtl.min.css"/>
  <link rel="stylesheet" href="jquery-ui/jquery-ui.min.css"/>
  <link rel="stylesheet" href="Manual_css-js/home.css"/>
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="Manual_css-js/home.js"></script>
  <script src="jquery-ui/jquery-ui.min.js"></script>
  
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <link rel="shortcut icon" href="images/car2.jpg" />
</head>
<body>

<div id="container-fluid-of-header" class="container-fluid">
	<div id="navbar-row" class="row">
		<!-- navbar of header -->
		<nav id="navbar" class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img id="logo" src="img/Caral.jpg"></a>
					
				</div>
				
				<div class="collapse navbar-collapse" id="mynavbar">
					<ul id="ul-nav" class="nav navbar-nav ">
						<li class=""><a href="home.php">صفحه اصلی</a></li>
						<li class=""><a href="manage_post.php">آگهی های من</a></li>
						<li class=""><a href="contact_us.php">تماس با ما</a></li>
						<li class=""><a href="about_us.php">درباره ما</a></li>
						
					</ul>
					<ul id="search-ul" class="nav navbar-nav navbar-left">
					
					<form action="search.php" method="get">
						<input name="search-key" id="search-head" type="text" class="form-control" placeholder="جستجوی کلیدواژه">
					</form>	
					</ul>
				</div>
			</div>
			
		</nav>
	</div>
	