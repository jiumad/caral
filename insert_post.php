﻿
<?php include("header.php"); ?>

<div id="main-page-content" class="container">
		
		
		<div class="row" style="">
			
			
			
				<div class="col col-lg-3 col-md-3"></div>
			
				<div id="div-panel" class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
					
					
					
					<div class="panel panel-default">
						<div class="panel-heading"> ایجاد پست </div>
						<div id="filter-panel" class="panel-body">
							<form  action="post_success.php" method="post" enctype="multipart/form-data" role="form">
								<div class="form-group ">
								
										<p>ایمیل</p>
										<input type="text" name="email" class="form-control" placeholder="a@b.com" required="required">
										<p>رمز مدیریت</p>
										<input type="password" name="manage_password" class="form-control" placeholder="a123" required="required">
										<p>تکرار رمز مدیریت</p>
										<input type="password" name="confirm_manage_password" class="form-control" placeholder="a123" required="required">
										<p>تلفن</p>
										<input type="text" name="phone" class="form-control" placeholder="****0919221" required="required">
										<p>عنوان آگهی</p>
										<input type="text" name="subject" class="form-control" placeholder="لپ تاپ اچ پی" required="required">
										<p>توضیحات</p>
										<textarea name="description" type="text" class="insert-post-description form-control"  placeholder="توضیحات" required="required"></textarea>
									
									<p>انتخاب استان</p>
									<select id="select-ostan" class="form-control" name="province">
										<?php
											$connection = mysqli_connect('localhost',$database_user,$database_pass,$database_name);
											mysqli_set_charset($connection,"utf8");
											$query_string = mysqli_query($connection,"SELECT * FROM province");
											while($result = mysqli_fetch_array($query_string)){
												$province_id = $result['p_id'];
												$name = $result['name'];
												
												echo "<option value=".$province_id.">".$name."</option>";
												
											}
										?>
										
									</select>
									
									
									<p>دسته بندی</p>
									<select id="select-type" class="form-control" name="category">
										<?php 
											$query_string = mysqli_query($connection,"SELECT * FROM category");
												while($result = mysqli_fetch_array($query_string)){
													$category_id = $result['c_id'];
													$name = $result['name'];
													
													echo "<option value=".$category_id.">".$name."</option>";
												}
												mysqli_close();
										?>
									</select>
									
		
									
									<p>
										<label for="price" style="font-size:1.2em; display:inline;">قیمت (تومان)</label>
										<section style="display:inline;">
										<input type="number" name="price" id="price" class="form-control" placeholder="1200000">
										</section>
									</p>
									
										<label class="Upload-caption" for="upload-image">آپلود عکس :</label> 
										<input type="file" id="upload-image" class="upload-image" name="ImageToUpload" accept="image/*">
										<input id="captcha" type="text" name="captcha" class="captcha" placeholder="عدد داخل کادر را وارد کنید" required="required" >
										<img id="img-captcha" src="captcha.php" class="captcha-img">
										
									<button name="submit" id="search-button-of-filter-panel" type="submit" class="btn btn-success pull-left">ثبت</button>
									
								</div>
								<?php
								if(isset($_GET['err'])){
									switch ($_GET['err']){
										case "1":
											echo "<p class='err'>کپچا را با دقت وارد کنید</p>";
											break;
										case "2":
											echo "<p class='err'>مقدار ورودی خالی است</p>";
											break;
										case "3":
											echo "<p class='err'>ورودی تعریف نشده است</p>";
											break;
										case "4":
											echo "<p class='err'>آپلود عکس ناقص است</p>";
											break;
										case "5":
											echo "<p class='err'>رمز مدیریتی و تکرار آن میبایست یکسان باشند</p>";
											break;
										case "6":
											echo "<p class='err'>درحال حاضر این رمز مدیریتی توسط شخص دیگری انتخاب شده،لطفا رمزی دیگر انتخاب نمایید</p>";
											break;
										default:
										 echo"";
									}
								}
								?>
							</form>
							
						</div>
					</div>
					
				</div>
				
				<div class="col col-lg-3 col-md-3"></div>
			
		</div>
		
		
		
		

 
 
 


	</div>	
	